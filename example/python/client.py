import json
import sys
from os import getenv

import requests
from flask import Flask, Response, request

app = Flask(__name__)

SERVER_ADDRESS = getenv('SERVER_ADDRESS', 'http://localhost:8080')
PARTICIPANT_ID = getenv('PARTICIPANT_ID')
if not PARTICIPANT_ID:
    print('Missing environment variable "PARTICIPANT_ID"', file=sys.stderr)
    sys.exit(1)


def answer(payload, id):
    requests.post(f'http://localhost:8080/rest/game/answer?participantId=2&questionId={id}',
                  headers={'Content-Type': 'application/json'}, json=payload)


@app.route('/', methods=('GET', 'POST'))
def index():
    data = request.get_json()
    question_id = int(request.args['questionId'])

    if isinstance(data, str):
        if 'ping' in data:
            answer('pong', question_id)
        elif 'Hello' in data:
            answer('Bye', question_id)
        elif '2 + 2' in data:
            answer(4, question_id)
    elif isinstance(data, list):
        answer((8, 8), question_id)

    return Response(json.dumps(''), mimetype='application/json', status=200)
