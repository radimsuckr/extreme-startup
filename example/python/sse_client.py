import json
import sys
from os import getenv

import requests
import sseclient

SERVER_ADDRESS = getenv('SERVER_ADDRESS', 'http://localhost:8080')
PARTICIPANT_ID = getenv('PARTICIPANT_ID')
if not PARTICIPANT_ID:
    print('Missing environment variable "PARTICIPANT_ID"', file=sys.stderr)
    sys.exit(1)

SUBSCRIBE_URL = f'{SERVER_ADDRESS}/rest/game/subscribe-events?participantId={PARTICIPANT_ID}'
ANSWER_URL = f'{SERVER_ADDRESS}/rest/game/answer?participantId={PARTICIPANT_ID}'


def answer(payload, id):
    resp = requests.post(f'{ANSWER_URL}&questionId={id}', headers={'Content-Type': 'application/json'}, json=payload)
    print(resp.status_code, resp.text)


resp = requests.get(SUBSCRIBE_URL, headers={'Accept': 'text/event-stream'}, stream=True)
if resp.status_code != 200:
    print('Failed to subscribe to SSE stream', file=sys.stderr)
    sys.exit(1)
client = sseclient.SSEClient(resp)

for event in client.events():
    if event.event != 'question':
        continue
    question_id = int(event.id)
    data = json.loads(event.data)
    print(question_id, data)

    if isinstance(data, str):
        if 'ping' in data:
            answer('pong', question_id)
        elif 'Hello' in data:
            answer('Bye', question_id)
        elif '2 + 2' in data:
            answer(4, question_id)
    elif isinstance(data, list):
        answer((8, 8), question_id)
