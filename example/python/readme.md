# How to run demo Python quesiton server and clients

Base setup steps:
 1. `cd example/python`
 2. `python -m virtualenv venv`
 3. `./venv/bin/activate`
 4. `pip install -r requirements.txt`

## Question server

Command for starting question server: `FLASK_APP=question_server.py flask run`

Command for starting question server with static questions from JSON file: `QUESTIONS_FROM_FILE=path/to/questions.json FLASK_APP=question_server.py flask run`

## Clients

Game server address can be specified via environment variable `SERVER_ADDRESS`. The default value is `http://localhost:8080`. You will also need to set `PARTICIPANT_ID` variable which is the ID of your participant account.

### HTTP client

To run demo HTTP client: `FLASK_APP=client.py flask run -p <client-port>`

### SSE client

To run demo SSE client: `python sse_client.py`
