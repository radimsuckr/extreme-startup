"""Simple implementation of G2Q API for demonstration purposes only."""
import json
import random
from os import getenv

from flask import Flask, Response, request

app = Flask(__name__)

QUESTIONS_FROM_FILE = getenv('QUESTIONS_FROM_FILE')

if QUESTIONS_FROM_FILE == 'yes':
    # Only questions with known static answers are allowed in JSON file
    with open('questions.json', 'r') as qf:
        questions = {int(k): v for k, v in json.load(qf).items()}
else:
    questions = {
        0: (0, json.dumps('ping'), json.dumps('pong'), 10),
        1: (0, json.dumps('Hello'), json.dumps('Bye'), 20),
        2: (1, json.dumps('2 + 2'), json.dumps(4), 30),
        3: (2, ({'eq': 'x + y', 'x': 2, 'y': 4}, {'eq': 'x + y', 'x': 4, 'y': 2}), lambda x: x['x'] * x['y'], 40),
    }
max_level = max((x[0] for x in questions.values()))


@app.route('/healthcheck')
def healthcheck():
    return ('', 200)


def make_json_response(payload, status):
    return Response(json.dumps(payload), status=status, mimetype='application/json')


def serialize_question(id, row):
    body = {
        'content': row[1],
        'id': id,
        'level': row[0],
        'points': row[3],
    }
    if isinstance(row[2], str):
        body.update({'expectedAnswers': row[2]})
    return body


def _get_question(question_id):
    question = questions.get(question_id)
    if not question:
        return make_json_response('', 404)
    else:
        return make_json_response(serialize_question(question_id, question), 200)


def _post_question(json_body):
    level = json_body['level']
    if json_body['answersCount'] > 0:
        ratio = json_body['correctAnswersCount'] / json_body['answersCount']

        if level == max_level and ratio > 0.8 and json_body['answersCount'] > 10:
            return make_json_response('', 204)
        elif level < max_level and ratio > 0.75 and json_body['answersCount'] > 5:
            level += 1

    id, row = random.choice([(id, row) for id, row in questions.items() if row[0] == level])
    return make_json_response(serialize_question(id, row), 200)


@app.route('/question', methods=('GET', 'POST'))
def question():
    if request.method == 'GET':
        question_id = int(request.args['questionId'])
        return _get_question(question_id)
    elif request.method == 'POST':
        return _post_question(request.get_json())


@app.route('/answer', methods=('POST',))
def answer():
    request_body = request.get_json()
    question_id = int(request_body['questionId'])
    question = questions[question_id]
    payload = json.loads(request_body['payload'])

    if not question_id or not isinstance(payload, list):
        return make_json_response('', 422)

    if not callable(question[2]):
        return make_json_response('', 400)
    else:
        if len(question[1]) != len(payload):
            return make_json_response('', 406)

        for ix, x in enumerate(question[1]):
            if question[2](x) != payload[ix]:
                return make_json_response('', 406)
    return make_json_response('', 200)
