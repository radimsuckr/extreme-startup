FROM maven:3-openjdk-11 AS build

WORKDIR /build

COPY pom.xml pom.xml
RUN mvn dependency:resolve

COPY src src
RUN mvn package


FROM payara/micro:5.2022.1-jdk11 AS app

COPY --from=build /build/target/extremestartup-1.0-SNAPSHOT.war /opt/payara/deployments/extremestartup.war

CMD ["--deploy", "/opt/payara/deployments/extremestartup.war", "--contextroot", "/"]
