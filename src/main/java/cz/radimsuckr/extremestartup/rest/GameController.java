package cz.radimsuckr.extremestartup.rest;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.sse.OutboundSseEvent;
import javax.ws.rs.sse.Sse;
import javax.ws.rs.sse.SseBroadcaster;
import javax.ws.rs.sse.SseEventSink;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.radimsuckr.extremestartup.Utils;
import cz.radimsuckr.extremestartup.concurrent.GameLoopController;
import cz.radimsuckr.extremestartup.concurrent.event.HttpQuestionEvent;
import cz.radimsuckr.extremestartup.concurrent.event.SseQuestionEvent;
import cz.radimsuckr.extremestartup.dto.QuestionApiQuestionDto;
import cz.radimsuckr.extremestartup.exception.AnswerShouldNotBeForwarded;
import cz.radimsuckr.extremestartup.exception.InvalidResponseFromQuestionServerException;
import cz.radimsuckr.extremestartup.exception.ParticipantReturnedInvalidResponseException;
import cz.radimsuckr.extremestartup.exception.ParticipantReturnedNoResponseException;
import cz.radimsuckr.extremestartup.exception.QuestionNotFoundException;
import cz.radimsuckr.extremestartup.model.Answer;
import cz.radimsuckr.extremestartup.model.CommunicationType;
import cz.radimsuckr.extremestartup.service.AnswerService;
import cz.radimsuckr.extremestartup.service.ParticipantService;

/**
 * JAX-RS controller which handles dispatching of new questions (either via HTTP
 * or SSE) and handles receiving and validation of answers.
 */
@ApplicationScoped
@Path("game")
public class GameController {

    private static final Logger logger = LoggerFactory.getLogger(GameController.class.getName());

    private Sse sse;

    private OutboundSseEvent.Builder eventBuilder;

    private Map<Long, SseBroadcaster> broadcasters = new HashMap<>();

    @EJB
    private ParticipantService participantService;

    @EJB
    private AnswerService answerService;

    @Inject
    private ParticipantApiClient participantApiClient;

    @Inject
    private QuestionApiClient questionApiClient;

    @Inject
    private GameLoopController gameLoopController;

    private static final Jsonb jsonb = JsonbBuilder.create();

    @Context
    public void setSse(Sse sse) {
        this.sse = sse;
        this.eventBuilder = sse.newEventBuilder();
    }

    @GET
    @Path("subscribe-events")
    @Produces(MediaType.SERVER_SENT_EVENTS)
    public void sseSubscribe(@QueryParam("participantId") Long participantId, @Context SseEventSink sink) {
        if (participantId == null) {
            throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
                    .entity("participantId parameter is mandatory").build()); // TODO: JSON error message
        }

        var participant = participantService.findById(participantId);
        if (participant == null) {
            throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
                    .entity("Participant with id " + participantId + " does not exist").build());
        }

        if (participant.getCommunicationType() != CommunicationType.SSE) {
            throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
                    .entity("You've opted for request-response communication").build());
        }

        logger.info("Registering a new SSE sink");
        var broadcaster = sse.newBroadcaster();
        broadcaster.onClose((closedSink) -> {
            logger.info("Closing SSE sink for participant \"" + participantId + "\"");
            broadcasters.remove(participantId);
        });
        broadcaster.onError((erroredSink, throwable) -> {
            logger.warn("Got an error on SSE sink for participant \"" + participantId + "\"" + throwable);
            broadcasters.remove(participantId);
        });
        broadcaster.register(sink);
        broadcasters.put(participantId, broadcaster);
    }

    @POST
    @Path("answer")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response submitAnswer(@QueryParam("participantId") Long participantId,
            @QueryParam("questionId") Long questionId, String payload) {
        if (!gameLoopController.isGameRunning()) {
            return Response.status(Response.Status.NOT_ACCEPTABLE)
                    .entity("Game is not running and thus not accepting any answers").build();
        }

        if (participantId == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Participant ID must be supplied").build();
        }

        if (questionId == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Question ID must be supplied").build();
        }

        logger.info("SSE ANSWER JSON: " + payload);
        var participant = participantService.findById(participantId);
        if (participant == null) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("Participant with id " + participantId + " does not exist").build();
        }

        QuestionApiQuestionDto question = null;
        try {
            question = questionApiClient.getQuestion(questionId);
        } catch (QuestionNotFoundException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity("There is no question with id " + questionId)
                    .build();
        } catch (InvalidResponseFromQuestionServerException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error getting question detail from question server").build();
        }

        var points = question.getPoints();
        var shouldForwardAnswer = question.getExpectedAnswers() == null;

        if (shouldForwardAnswer) {
            logger.info("Forwarding answer");
            try {
                var isCorrect = questionApiClient.answer(question.getId(), payload);
                points = Utils.calculatePoints(points, isCorrect);
            } catch (AnswerShouldNotBeForwarded e) {
                e.printStackTrace();
                logger.error("Answer for question with id \"" + question.getId() + "\" should not be forwared");
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Answer should not be forwarded")
                        .build();
            }
        } else {
            logger.info("Local validation");
            var isCorrect = payload.equals(question.getExpectedAnswers());
            points = Utils.calculatePoints(points, isCorrect);
        }

        logger.info("Persisting answer " + points);
        var answer = new Answer(question.getId(), question.getLevel(), payload, points, participant);
        answerService.persist(answer);

        return Response.status(Response.Status.OK).build();
    }

    public void onHttpQuestionEvent(@Observes HttpQuestionEvent event) {
        logger.info("Caught HttpQuestionEvent");

        var participant = participantService.findById(event.getParticipantId());
        logger.info("Found participant " + participant.getId());

        QuestionApiQuestionDto question = null;
        try {
            question = questionApiClient.getQuestion(event.getQuestionId());
        } catch (QuestionNotFoundException e) {
            e.printStackTrace();
        } catch (InvalidResponseFromQuestionServerException e) {
            e.printStackTrace();
        }

        var payload = question.getContent();

        try {
            logger.info("Asking a question");
            participantApiClient.askQuestion(participant, payload, question.getId());
        } catch (ParticipantReturnedNoResponseException e) {
            logger.error("Participant \"" + event.getParticipantId() + "\" returned no response");
            e.printStackTrace();
        } catch (ParticipantReturnedInvalidResponseException e) {
            logger.error("Participant \"" + event.getParticipantId() + "\" returned invalid response");
            e.printStackTrace();
        }
    }

    public void onSseQuestionEvent(@Observes SseQuestionEvent event) {
        logger.info("Triggered onQuestionEvent");

        var participant = participantService.findById(event.getParticipantId());
        QuestionApiQuestionDto question = null;
        try {
            question = questionApiClient.getQuestion(event.getQuestionId());
        } catch (QuestionNotFoundException e) {
            e.printStackTrace();
        } catch (InvalidResponseFromQuestionServerException e) {
            e.printStackTrace();
        }

        var broadcaster = broadcasters.get(participant.getId());
        if (broadcaster == null) {
            // TODO: Participant chose SSE but doesn't have any active sink
            logger.warn("No SSE broadcaster is registered for participantId " + event.getParticipantId());
            return;
        }

        var sseEvent = eventBuilder.name("question").id(question.getId().toString())
                .data(String.class, jsonb.toJson(question.getContent())).build();
        broadcaster.broadcast(sseEvent);
    }
}
