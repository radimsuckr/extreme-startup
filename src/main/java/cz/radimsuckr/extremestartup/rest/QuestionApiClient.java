package cz.radimsuckr.extremestartup.rest;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.radimsuckr.extremestartup.dto.AnswerDto;
import cz.radimsuckr.extremestartup.dto.QuestionApiParticipantDto;
import cz.radimsuckr.extremestartup.dto.QuestionApiQuestionDto;
import cz.radimsuckr.extremestartup.exception.AnswerShouldNotBeForwarded;
import cz.radimsuckr.extremestartup.exception.InvalidResponseFromQuestionServerException;
import cz.radimsuckr.extremestartup.exception.NoMoreQuestionsForParticipantException;
import cz.radimsuckr.extremestartup.exception.QuestionNotFoundException;

/**
 * Client class for Question server API.
 */
@ApplicationScoped
@Named
public class QuestionApiClient {

    private static final Logger logger = LoggerFactory.getLogger(QuestionApiClient.class.getName());

    @Inject
    @ConfigProperty(name = "questionServerUrl", defaultValue = "http://localhost:5000")
    private String questionServerUrl;

    private final Client client = ClientBuilder.newClient();

    private WebTarget baseTarget;

    private WebTarget healthcheckTarget;

    private WebTarget questionTarget;

    private WebTarget answerTarget;

    @PostConstruct
    public void init() {
        baseTarget = client.target(questionServerUrl);
        healthcheckTarget = baseTarget.path("/healthcheck");
        questionTarget = baseTarget.path("/question");
        answerTarget = baseTarget.path("/answer");
    }

    public boolean isServerHealthy() {
        try {
            var response = healthcheckTarget.request(MediaType.TEXT_PLAIN).get();
            return response.getStatus() == 200;
        } catch (WebApplicationException | ProcessingException e) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    public QuestionApiQuestionDto getQuestion(long questionId)
            throws QuestionNotFoundException, InvalidResponseFromQuestionServerException {
        try {
            var response = questionTarget.queryParam("questionId", questionId).request().get();
            if (response == null) {
                logger.error("Failed to get question detail");
            } else {
                logger.info("Response = " + response);
            }

            if (response.getStatus() == Status.OK.getStatusCode() && response.hasEntity()) {
                return response.readEntity(QuestionApiQuestionDto.class);
            } else if (response.getStatus() == Status.NOT_FOUND.getStatusCode()) {
                throw new QuestionNotFoundException();
            } else {
                throw new InvalidResponseFromQuestionServerException();
            }
        } catch (WebApplicationException | ProcessingException e) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    public QuestionApiQuestionDto getQuestion(QuestionApiParticipantDto participantDto)
            throws NoMoreQuestionsForParticipantException, InvalidResponseFromQuestionServerException {
        try {
            var response = questionTarget.request(MediaType.APPLICATION_JSON).post(Entity.json(participantDto));
            if (response == null) {
                logger.error("Failed to get next question");
            } else {
                logger.info("Response = " + response);
            }

            if (response.getStatus() == Status.NO_CONTENT.getStatusCode()) {
                throw new NoMoreQuestionsForParticipantException(participantDto.getName());
            } else if (response.getStatus() == Status.OK.getStatusCode() && response.hasEntity()) {
                return response.readEntity(QuestionApiQuestionDto.class);
            } else {
                throw new InvalidResponseFromQuestionServerException();
            }
        } catch (WebApplicationException | ProcessingException e) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    public boolean answer(Long questionId, String answer) throws AnswerShouldNotBeForwarded {
        try {
            var answerDto = new AnswerDto(questionId, answer);
            var response = answerTarget.request(MediaType.APPLICATION_JSON).post(Entity.json(answerDto),
                    Response.class);
            switch (response.getStatus()) {
                case 200:
                    return true;
                case 400:
                    throw new AnswerShouldNotBeForwarded();
                case 406:
                default:
                    return false;
            }
        } catch (WebApplicationException | ProcessingException e) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }
    }
}
