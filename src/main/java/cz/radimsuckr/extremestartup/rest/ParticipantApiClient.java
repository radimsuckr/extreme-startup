package cz.radimsuckr.extremestartup.rest;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.radimsuckr.extremestartup.exception.ParticipantReturnedInvalidResponseException;
import cz.radimsuckr.extremestartup.exception.ParticipantReturnedNoResponseException;
import cz.radimsuckr.extremestartup.model.Participant;

/**
 * Abstracts away sending HTTP requests to participants. Used for sending
 * questions via HTTP.
 */
@ApplicationScoped
public class ParticipantApiClient {

    private static final Logger logger = LoggerFactory.getLogger(ParticipantApiClient.class.getName());

    private final Client client = ClientBuilder.newClient();

    public void askQuestion(Participant participant, Object questionBody, long questionId)
            throws ParticipantReturnedNoResponseException, ParticipantReturnedInvalidResponseException {
        var url = "http://" + participant.getIpAddress() + ":" + participant.getPort();
        var target = client.target(url);
        try {
            logger.info("Asking for an answer on \"" + url + "\"");
            var response = target.queryParam("questionId", questionId).request(MediaType.APPLICATION_JSON)
                    .post(Entity.json(questionBody));
            if (response.getStatus() != Response.Status.OK.getStatusCode()) {
                throw new ParticipantReturnedInvalidResponseException();
            }
        } catch (WebApplicationException e) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        } catch (ProcessingException e) {
            logger.warn("Failed to ask participant \"" + url + "\" a question");
            throw new ParticipantReturnedNoResponseException();
        }
    }
}
