package cz.radimsuckr.extremestartup.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Main JAX-RS application class.
 */
@ApplicationPath("rest")
public class RestApplication extends Application {
}
