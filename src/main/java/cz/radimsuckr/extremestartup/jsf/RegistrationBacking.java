package cz.radimsuckr.extremestartup.jsf;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.radimsuckr.extremestartup.exception.ParticipantOnSameIpAddressAndPortExists;
import cz.radimsuckr.extremestartup.exception.ParticipantWithSameUsernameExistsException;
import cz.radimsuckr.extremestartup.model.CommunicationType;
import cz.radimsuckr.extremestartup.model.Participant;
import cz.radimsuckr.extremestartup.service.ParticipantService;

/**
 * JSF backing class for registration view.
 */
@RequestScoped
@Named
public class RegistrationBacking {

    private static final Logger logger = LoggerFactory.getLogger(RegistrationBacking.class.getName());

    private Participant registrationParticipant;

    @EJB
    private ParticipantService participantService;

    public RegistrationBacking() {
        registrationParticipant = new Participant();
    }

    public Participant getRegistrationParticipant() {
        return registrationParticipant;
    }

    public void setRegistrationParticipant(Participant registrationParticipant) {
        this.registrationParticipant = registrationParticipant;
    }

    public CommunicationType[] getCommunicationTypes() {
        return CommunicationType.values();
    }

    public String register() {
        try {
            participantService.persist(registrationParticipant);
        } catch (ParticipantWithSameUsernameExistsException e) {
            logger.warn("Registration of participant %s failed. Participant with the same username already exists.",
                    registrationParticipant);
            var msg = "Username already taken";
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg));
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            return "registration.jsf?faces-redirect=true";
        } catch (ParticipantOnSameIpAddressAndPortExists e) {
            logger.warn(
                    "Registration of participant %s failed. Participant with the same IP address and port already exists.",
                    registrationParticipant);
            var msg = "IP address and port already taken";
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg));
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            return "registration.jsf?faces-redirect=true";
        }

        logger.info("Registered participant %s", registrationParticipant);
        var msg = "Participant registered successfully";
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msg));
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

        return "registration.jsf?faces-redirect=true";
    }
}
