package cz.radimsuckr.extremestartup.jsf;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.HorizontalBarChartModel;

import cz.radimsuckr.extremestartup.service.AnswerService;

/**
 * JSF widget constructing a bar chart view used on the index page.
 */
@RequestScoped
@Named
public class PointsChartView implements Serializable {

    @EJB
    private AnswerService answerService;

    private HorizontalBarChartModel chartModel;

    @PostConstruct
    public void init() {
        chartModel = new HorizontalBarChartModel();

        var points = new ChartSeries();
        points.setLabel("Participants");

        answerService.sumPoints().forEach(points::set);

        chartModel.addSeries(points);
        chartModel.setTitle("Participant points");

        var xAxis = chartModel.getAxis(AxisType.X);
        xAxis.setLabel("Points");
        xAxis.setMin(0);

        var yAxis = chartModel.getAxis(AxisType.Y);
        yAxis.setLabel("Teams");
    }

    public HorizontalBarChartModel getModel() {
        return chartModel;
    }
}
