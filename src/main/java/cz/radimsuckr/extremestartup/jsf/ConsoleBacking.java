package cz.radimsuckr.extremestartup.jsf;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.radimsuckr.extremestartup.concurrent.GameLoopController;

/**
 * JSF backing class for management console.
 */
@RequestScoped
@Named
public class ConsoleBacking {

    private static final Logger logger = LoggerFactory.getLogger(ConsoleBacking.class.getName());

    @Inject
    private GameLoopController gameLoopController;

    public void toggleGameLoop() {
        if (gameLoopController.isGameRunning()) {
            logger.info("Stopping game loop");
            gameLoopController.stop();
        } else {
            logger.info("Starting game loop");
            gameLoopController.start();
        }
    }

    public boolean getGameLoopState() {
        return gameLoopController.isGameRunning();
    }
}
