package cz.radimsuckr.extremestartup.jsf;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import cz.radimsuckr.extremestartup.service.ParticipantService;

/**
 * JSF backing class for index page (dashboard).
 */
@RequestScoped
@Named
public class IndexBacking {

    public static class ParticipantData {

        private final String text;

        private final boolean isFinished;

        public ParticipantData(String text, boolean isFinished) {
            this.text = text;
            this.isFinished = isFinished;
        }

        public String getText() {
            return text;
        }

        public boolean getIsFinished() {
            return isFinished;
        }
    }

    @EJB
    private ParticipantService participantService;

    public List<ParticipantData> getParticipantDataList() {
        return participantService.getParticipantDataListForIndexBacking();
    }
}
