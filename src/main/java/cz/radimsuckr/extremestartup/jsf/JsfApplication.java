package cz.radimsuckr.extremestartup.jsf;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.annotation.FacesConfig;

/**
 * Main JSF application class.
 */
@ApplicationScoped
@FacesConfig
public class JsfApplication {
}
