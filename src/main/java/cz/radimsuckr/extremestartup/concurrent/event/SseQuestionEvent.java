package cz.radimsuckr.extremestartup.concurrent.event;

/**
 * Event class for handler of dispatched questions using SSE.
 */
public class SseQuestionEvent extends QuestionEvent {

    public SseQuestionEvent(long participantId, long questionId) {
        super(participantId, questionId);
    }
}
