package cz.radimsuckr.extremestartup.concurrent.event;

/**
 * Base class for question events.
 */
public abstract class QuestionEvent {

    private long participantId;

    private long questionId;

    public QuestionEvent(long participantId, long questionId) {
        this.participantId = participantId;
        this.questionId = questionId;
    }

    public long getParticipantId() {
        return participantId;
    }

    public long getQuestionId() {
        return questionId;
    }
}
