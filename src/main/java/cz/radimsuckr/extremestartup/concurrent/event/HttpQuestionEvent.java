package cz.radimsuckr.extremestartup.concurrent.event;

/**
 * Event class for handler of dispatched questions using plain HTTP.
 */
public class HttpQuestionEvent extends QuestionEvent {

    public HttpQuestionEvent(long participantId, long questionId) {
        super(participantId, questionId);
    }
}
