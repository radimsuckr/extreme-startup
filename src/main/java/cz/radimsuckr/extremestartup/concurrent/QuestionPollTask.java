package cz.radimsuckr.extremestartup.concurrent;

import javax.enterprise.event.Event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.radimsuckr.extremestartup.concurrent.event.HttpQuestionEvent;
import cz.radimsuckr.extremestartup.concurrent.event.SseQuestionEvent;
import cz.radimsuckr.extremestartup.dto.QuestionApiQuestionDto;
import cz.radimsuckr.extremestartup.exception.InvalidResponseFromQuestionServerException;
import cz.radimsuckr.extremestartup.exception.NoMoreQuestionsForParticipantException;
import cz.radimsuckr.extremestartup.model.Participant;
import cz.radimsuckr.extremestartup.rest.QuestionApiClient;
import cz.radimsuckr.extremestartup.service.ParticipantService;

/**
 * A runnable that is used by {@link QuestionPollBean}. Contains logic for
 * dispatching questions.
 */
public class QuestionPollTask implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(QuestionPollTask.class.getName());

    private final ParticipantService participantService;

    private final Long participantId;

    private final QuestionApiClient questionApiClient;

    private final Event<SseQuestionEvent> sseQuestionEvent;

    private final Event<HttpQuestionEvent> httpQuestionEvent;

    public QuestionPollTask(ParticipantService participantService, Long participantId,
            QuestionApiClient questionApiClient, Event<SseQuestionEvent> sseQuestionEvent,
            Event<HttpQuestionEvent> httpQuestionEvent) {
        this.participantService = participantService;
        this.participantId = participantId;
        this.questionApiClient = questionApiClient;
        this.sseQuestionEvent = sseQuestionEvent;
        this.httpQuestionEvent = httpQuestionEvent;
    }

    protected void poll() {
        var participant = participantService.findById(participantId);
        if (participant == null) {
            var errorMsg = "Participant " + participantId + " does not exist";
            logger.error(errorMsg);
            throw new RuntimeException(errorMsg);
        }
        logger.info("Participant " + participant.getId() + " found");

        var participantDto = participantService.getQuestionServerMetadata(participant);
        try {
            var question = questionApiClient.getQuestion(participantDto);
            sendNewQuestionEvent(participant, question);
        } catch (NoMoreQuestionsForParticipantException e) {
            logger.info("No more questions for participant " + participant.getId());
            participantService.markAsFinished(participant);
        } catch (InvalidResponseFromQuestionServerException e) {
            logger.error("Question server returned invalid response");
        }
    }

    protected void sendNewQuestionEvent(Participant recipient, QuestionApiQuestionDto question) {
        // TODO: JDK 17 improvement: switch expressions (for exhaustive switch)
        switch (recipient.getCommunicationType()) {
            case REQUEST_RESPONSE:
                logger.info("Sending new HTTP question " + question.getId() + " to " + recipient.getId());
                httpQuestionEvent.fire(new HttpQuestionEvent(recipient.getId(), question.getId()));
                break;
            case SSE:
                logger.info("Sending new SSE question " + question.getId() + " to " + recipient.getId());
                sseQuestionEvent.fire(new SseQuestionEvent(recipient.getId(), question.getId()));
            default:
                break;
        }
    }

    @Override
    public void run() {
        poll();
    }
}
