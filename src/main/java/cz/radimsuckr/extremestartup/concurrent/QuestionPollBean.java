package cz.radimsuckr.extremestartup.concurrent;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.radimsuckr.extremestartup.concurrent.event.HttpQuestionEvent;
import cz.radimsuckr.extremestartup.concurrent.event.SseQuestionEvent;
import cz.radimsuckr.extremestartup.model.Participant;
import cz.radimsuckr.extremestartup.rest.QuestionApiClient;
import cz.radimsuckr.extremestartup.service.ParticipantService;

/**
 * Handles spawning of additional threads which dispatch questions to the
 * participants.
 */
@ApplicationScoped
public class QuestionPollBean {

    private static final Logger logger = LoggerFactory.getLogger(QuestionPollBean.class.getName());

    @Resource
    private ManagedExecutorService executorService;

    @EJB
    private ParticipantService participantService;

    @Inject
    private QuestionApiClient questionApiClient;

    @Inject
    private Event<SseQuestionEvent> sseQuestionEvent;

    @Inject
    private Event<HttpQuestionEvent> httpQuestionEvent;

    public void start() {
        participantService.findUnfinished().forEach(this::start);
    }

    private void start(Participant participant) {
        logger.info("Creating a new poll task for participantId " + participant.getId());
        var task = new QuestionPollTask(participantService, participant.getId(), questionApiClient, sseQuestionEvent,
                httpQuestionEvent);

        logger.info("Starting a task for participantId " + participant.getId());
        executorService.submit(task);
    }
}
