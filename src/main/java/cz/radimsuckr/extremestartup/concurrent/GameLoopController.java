package cz.radimsuckr.extremestartup.concurrent;

import java.util.concurrent.atomic.AtomicBoolean;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.concurrent.ManagedThreadFactory;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handle starting and stopping of a thread that runs the main game loop in
 * background.
 */
@Singleton
@Startup
public class GameLoopController {

    private class LoopTask implements Runnable {

        private Thread thread;

        private final AtomicBoolean isRunning = new AtomicBoolean(false);

        private int sleepInterval;

        public LoopTask(int sleepInterval) {
            this.sleepInterval = sleepInterval;
        }

        public void start() {
            thread = threadFactory.newThread(this);
            thread.start();
        }

        public void stop() {
            isRunning.set(false);
        }

        public boolean isRunning() {
            return isRunning.get();
        }

        @Override
        public void run() {
            isRunning.set(true);
            while (isRunning.get()) {
                questionPollBean.start();
                try {
                    Thread.sleep(sleepInterval);
                } catch (InterruptedException e) {
                    thread.interrupt();
                    e.printStackTrace();
                }
            }
        }
    }

    private static final Logger logger = LoggerFactory.getLogger(GameLoopController.class.getName());

    @Resource
    private ManagedThreadFactory threadFactory;

    @Inject
    private QuestionPollBean questionPollBean;

    private LoopTask task;

    @PostConstruct
    public void init() {
        task = new LoopTask(1000);
    }

    public boolean isGameRunning() {
        return task.isRunning();
    }

    public void start() {
        logger.info("Starting game loop worker thread");
        task.start();
    }

    public void stop() {
        logger.info("Stopping game loop worker thread");
        task.stop();
    }
}
