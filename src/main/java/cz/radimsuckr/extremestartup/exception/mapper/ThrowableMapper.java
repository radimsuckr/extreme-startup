package cz.radimsuckr.extremestartup.exception.mapper;

import javax.json.bind.JsonbBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.radimsuckr.extremestartup.exception.dto.ErrorInfoDto;

/**
 * Exception handler implementation for JAX-RS.
 */
@Provider
public class ThrowableMapper implements ExceptionMapper<Throwable> {

    private static final Logger logger = LoggerFactory.getLogger(ThrowableMapper.class.getName());

    @Override
    public Response toResponse(Throwable t) {
        var jsonBuilder = JsonbBuilder.create();
        logger.error(t.getMessage());

        var info = new ErrorInfoDto("An unknown error happened. Admins can go and look in application logs.");

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(jsonBuilder.toJson(info))
                .type(MediaType.APPLICATION_JSON).build();
    }
}
