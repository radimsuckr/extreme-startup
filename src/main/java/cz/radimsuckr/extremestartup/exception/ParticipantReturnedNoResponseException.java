package cz.radimsuckr.extremestartup.exception;

/**
 * Represents an error state when participant didn't respond to dispatched
 * question at all.
 */
public class ParticipantReturnedNoResponseException extends AppException {
}
