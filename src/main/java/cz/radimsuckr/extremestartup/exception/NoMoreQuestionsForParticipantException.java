package cz.radimsuckr.extremestartup.exception;

/**
 * Represents a state when participant finished the game and the question server
 * does not have any more questions for them.
 */
public class NoMoreQuestionsForParticipantException extends AppException {

    public NoMoreQuestionsForParticipantException(String participant) {
        super("No more questions for participant \"" + participant + "\"");
    }
}
