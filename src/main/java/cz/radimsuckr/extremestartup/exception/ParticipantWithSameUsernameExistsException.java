package cz.radimsuckr.extremestartup.exception;

/**
 * Represents an error state when a new participant trying to register into the
 * game tries to use already taken username.
 */
public class ParticipantWithSameUsernameExistsException extends AppException {
}
