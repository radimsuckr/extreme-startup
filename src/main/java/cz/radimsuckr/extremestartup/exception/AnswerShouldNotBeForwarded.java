package cz.radimsuckr.extremestartup.exception;

/**
 * Represents an error state when forwarding an invalid question for validation
 * from question server.
 */
public class AnswerShouldNotBeForwarded extends AppException {
}
