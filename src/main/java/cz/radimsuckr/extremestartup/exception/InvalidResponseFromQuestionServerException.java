package cz.radimsuckr.extremestartup.exception;

/**
 * Represents an error state when question server returns invalid response.
 */
public class InvalidResponseFromQuestionServerException extends AppException {
}
