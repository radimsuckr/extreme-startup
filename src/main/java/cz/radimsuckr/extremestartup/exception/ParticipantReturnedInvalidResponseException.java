package cz.radimsuckr.extremestartup.exception;

/**
 * Represents an error state when participant didn't properly receive dispatched
 * question.
 */
public class ParticipantReturnedInvalidResponseException extends AppException {
}
