package cz.radimsuckr.extremestartup.exception.dto;

/**
 * DTO used for exception mapper {@link ThrowableMapper}.
 */
public class ErrorInfoDto {

    private String errorMessage;

    public ErrorInfoDto(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
