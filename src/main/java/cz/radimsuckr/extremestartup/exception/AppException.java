package cz.radimsuckr.extremestartup.exception;

/**
 * Base exception class for the application.
 */
public abstract class AppException extends Exception {

    public AppException() {
    }

    public AppException(String message) {
        super(message);
    }
}
