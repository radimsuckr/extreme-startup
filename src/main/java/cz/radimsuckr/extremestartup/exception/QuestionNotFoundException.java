package cz.radimsuckr.extremestartup.exception;

/**
 * Represents an error state when asking question server for details about a
 * specific question and the question does not exist.
 */
public class QuestionNotFoundException extends AppException {
}
