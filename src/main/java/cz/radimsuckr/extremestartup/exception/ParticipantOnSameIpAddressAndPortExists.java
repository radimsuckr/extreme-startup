package cz.radimsuckr.extremestartup.exception;

/**
 * Represents an error state when a new participant trying to register into the
 * game tries to use already taken IP address and port pair.
 */
public class ParticipantOnSameIpAddressAndPortExists extends AppException {
}
