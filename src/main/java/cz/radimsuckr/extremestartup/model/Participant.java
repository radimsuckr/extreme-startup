package cz.radimsuckr.extremestartup.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

/**
 * JPA entity representing participants.
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "Participant.countByIpAddressAndPort", query = "SELECT COUNT(p) FROM Participant p WHERE p.ipAddress = :ipAddress AND p.port = :port"),
        @NamedQuery(name = "Participant.countByUsername", query = "SELECT COUNT(p) FROM Participant p WHERE p.username = :username"),
        @NamedQuery(name = "Participant.findAll", query = "SELECT p FROM Participant p"),
        @NamedQuery(name = "Participant.findById", query = "SELECT p FROM Participant p WHERE p.id = :id"),
        @NamedQuery(name = "Participant.findUnfinished", query = "SELECT p FROM Participant p WHERE p.isFinished = false"),
})
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = { "ipAddress", "port" }),
})
public class Participant {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, unique = true)
    private String username;

    @Column(name = "ipAddress", nullable = false)
    @Pattern(regexp = "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$", message = "Invalid IPv4 address")
    // Regex pattern taken from https://stackoverflow.com/a/5667417/13169919
    private String ipAddress;

    @Column(name = "port", nullable = false)
    @Min(value = 1024, message = "Port must be greater than 1023")
    @Max(value = 65535, message = "Port must be less than 65536")
    private Integer port;

    @OneToMany(mappedBy = "participant", orphanRemoval = true)
    private List<Answer> answers = new ArrayList<>();

    @Column(nullable = false, unique = false)
    private Boolean isFinished = false;

    @Column(nullable = false, unique = false)
    @Enumerated(EnumType.STRING)
    private CommunicationType communicationType;

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void addAnswer(Answer answer) {
        answers.add(answer);
        answer.setParticipant(this);
    }

    public void markAsFinished() {
        isFinished = true;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public void setCommunicationType(CommunicationType type) {
        communicationType = type;
    }

    public CommunicationType getCommunicationType() {
        return communicationType;
    }

    @Override
    public String toString() {
        return "Participant{" + "username='" + username + '\'' + ", ipAddress='" + ipAddress + '\'' + ", port='" + port
                + '\'' + '}';
    }
}
