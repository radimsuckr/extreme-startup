package cz.radimsuckr.extremestartup.model;

/**
 * Represents communication preferences chosen by participants.
 */
public enum CommunicationType {
    REQUEST_RESPONSE,
    SSE,
}
