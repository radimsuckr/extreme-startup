package cz.radimsuckr.extremestartup.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * JPA entity representing participant's answers.
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "Answer.countByParticipantAndLevel", query = "SELECT COUNT(a) FROM Answer a WHERE a.participant = :participant AND a.questionLevel = :level"),
        @NamedQuery(name = "Answer.countCorrectAnswersUntilAnswerIdByParticipant", query = "SELECT COUNT(a) FROM Answer a WHERE a.participant = :participant AND a.points > 0 AND a.questionLevel = :level AND a.id > :answerId"),
        @NamedQuery(name = "Answer.countCorrectByParticipantAndLevel", query = "SELECT COUNT(a) FROM Answer a WHERE a.participant = :participant AND a.points > 0 AND a.questionLevel = :level"),
        @NamedQuery(name = "Answer.findCorrectByParticipantAndLevel", query = "SELECT a FROM Answer a WHERE a.participant = :participant AND a.questionLevel = :level"),
        @NamedQuery(name = "Answer.findCurrentLevelByParticipant", query = "SELECT MAX(a.questionLevel) FROM Answer a WHERE a.participant = :participant"),
        @NamedQuery(name = "Answer.findLastQuestionIdByParticipant", query = "SELECT a.questionId FROM Answer a WHERE a.participant = :participant ORDER BY a.id"),
        @NamedQuery(name = "Answer.findLatestWrongAnswerByParticipantAndLevel", query = "SELECT a FROM Answer a WHERE a.participant = :participant AND a.points <= 0 AND a.questionLevel = :level ORDER BY a.id DESC"),
        @NamedQuery(name = "Answer.sumPoints", query = "SELECT NEW cz.radimsuckr.extremestartup.dao.AnswerDao.UsernamePointsPair(p.username, COALESCE(SUM(a.points), 0)) FROM Participant p LEFT JOIN Answer a ON a.participant = p GROUP BY p.username"),
        @NamedQuery(name = "Answer.sumPointsByParticipant", query = "SELECT COALESCE(SUM(a.points), 0) FROM Answer a WHERE a.participant = :participant"),
})
public class Answer {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, unique = false)
    private Long questionId;

    @Column(nullable = false, unique = false)
    private Long questionLevel;

    @ManyToOne
    private Participant participant;

    @Column(nullable = false, unique = false)
    private String content;

    @Column(nullable = false, unique = false)
    private Integer points;

    @Column(nullable = false, unique = false)
    private Boolean isCorrect;

    public Answer() {
    }

    public Answer(Long questionId, Long questionLevel, String content, Integer points, Participant participant) {
        setQuestionId(questionId);
        setQuestionLevel(questionLevel);
        setContent(content);
        setPoints(points);
        setIsCorrect(points > 0);
        setParticipant(participant);
        participant.addAnswer(this);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public Long getQuestionLevel() {
        return questionLevel;
    }

    public void setQuestionLevel(Long questionLevel) {
        this.questionLevel = questionLevel;
    }

    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Boolean isCorrect() {
        return isCorrect;
    }

    public void setIsCorrect(boolean isCorrect) {
        this.isCorrect = isCorrect;
    }
}
