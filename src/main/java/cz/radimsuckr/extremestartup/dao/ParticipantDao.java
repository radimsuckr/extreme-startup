package cz.radimsuckr.extremestartup.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import cz.radimsuckr.extremestartup.model.Participant;

/**
 * Simple DAO class for Participant entity.
 */
@Stateless
public class ParticipantDao {

    @PersistenceContext
    private EntityManager em;

    public List<Participant> findAll() {
        var query = em.createNamedQuery("Participant.findAll", Participant.class);
        return query.getResultList();
    }

    public Participant findById(Long id) {
        var query = em.createNamedQuery("Participant.findById", Participant.class);
        query.setParameter("id", id);
        return query.getResultStream().findFirst().orElse(null);
    }

    public List<Participant> findUnfinished() {
        var query = em.createNamedQuery("Participant.findUnfinished", Participant.class);
        return query.getResultList();
    }

    public Long countByUsername(String username) {
        var query = em.createNamedQuery("Participant.countByUsername", Long.class);
        query.setParameter("username", username);
        return query.getSingleResult();
    }

    public Long countByIpAddressAndPort(String ipAddress, Integer port) {
        var query = em.createNamedQuery("Participant.countByIpAddressAndPort", Long.class);
        query.setParameter("ipAddress", ipAddress);
        query.setParameter("port", port);
        return query.getSingleResult();
    }

    public Participant merge(Participant participant) {
        return em.merge(participant);
    }

    public void persist(Participant participant) {
        em.persist(participant);
    }
}
