package cz.radimsuckr.extremestartup.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import cz.radimsuckr.extremestartup.model.Answer;
import cz.radimsuckr.extremestartup.model.Participant;

/**
 * Simple DAO class for Answer entity.
 */
@Stateless
public class AnswerDao {

    public static class UsernamePointsPair {

        public String username;

        public long points;

        public UsernamePointsPair(String username, long points) {
            this.username = username;
            this.points = points;
        }
    }

    @PersistenceContext
    private EntityManager em;

    public Long countByParticipantAndLevel(Participant participant, long level) {
        var query = em.createNamedQuery("Answer.countByParticipantAndLevel", Long.class);
        query.setParameter("participant", participant);
        query.setParameter("level", level);
        return query.getSingleResult();
    }

    public Long countCorrectAnswersUntilAnswerIdByParticipant(Participant participant, long level, long answerId) {
        var query = em.createNamedQuery("Answer.countCorrectAnswersUntilAnswerIdByParticipant", Long.class);
        query.setParameter("participant", participant);
        query.setParameter("level", level);
        query.setParameter("answerId", answerId);
        return query.getSingleResult();
    }

    public Long countCorrectByParticipantAndLevel(Participant participant, long level) {
        var query = em.createNamedQuery("Answer.countCorrectByParticipantAndLevel", Long.class);
        query.setParameter("participant", participant);
        query.setParameter("level", level);
        return query.getSingleResult();
    }

    public List<Answer> findCorrectByParticipantAndLevel(Participant participant, Long level) {
        var query = em.createNamedQuery("Answer.findCorrectByParticipantAndLevel", Answer.class);
        query.setParameter("participant", participant);
        query.setParameter("level", level);
        return query.getResultList();
    }

    public Long findCurrentLevelByParticipant(Participant participant) {
        var query = em.createNamedQuery("Answer.findCurrentLevelByParticipant", Long.class);
        query.setParameter("participant", participant);
        return query.getResultStream().filter(Objects::nonNull).findFirst().orElse(0L);
    }

    public Long findLastQuestionIdByParticipant(Participant participant) {
        var query = em.createNamedQuery("Answer.findLastQuestionIdByParticipant", Long.class);
        query.setMaxResults(1);
        query.setParameter("participant", participant);
        return query.getResultStream().findFirst().orElse(null);
    }

    public Answer findLatestWrongAnswerByParticipant(Participant participant, long level) {
        var query = em.createNamedQuery("Answer.findLatestWrongAnswerByParticipantAndLevel", Answer.class);
        query.setMaxResults(1);
        query.setParameter("participant", participant);
        query.setParameter("level", level);
        return query.getResultStream().findFirst().orElse(null);
    }

    public Map<String, Long> sumPoints() {
        var query = em.createNamedQuery("Answer.sumPoints", UsernamePointsPair.class);
        var results = query.getResultList();

        var data = new HashMap<String, Long>();
        results.forEach(item -> data.put(item.username, item.points));
        return data;
    }

    public long sumPointsByParticipant(Participant participant) {
        var query = em.createNamedQuery("Answer.sumPointsByParticipant", Long.class);
        query.setParameter("participant", participant);
        return query.getSingleResult();
    }

    public void persist(Answer answer) {
        em.persist(answer);
    }
}
