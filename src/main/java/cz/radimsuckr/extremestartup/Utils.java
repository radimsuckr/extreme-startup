package cz.radimsuckr.extremestartup;

import java.util.function.BiFunction;

/**
 * Simple class for various utility functions.
 */
public class Utils {

    private static BiFunction<Integer, Boolean, Integer> halveIncorrectAnswer = (points,
            isCorrect) -> isCorrect ? points : (int) Math.ceil((double) -1 / 2 * points);

    private static BiFunction<Integer, Boolean, Integer> f = Utils.halveIncorrectAnswer;

    private Utils() {
    }

    public static Integer calculatePoints(Integer points, Boolean isCorrect) {
        return f.apply(points, isCorrect);
    }
}
