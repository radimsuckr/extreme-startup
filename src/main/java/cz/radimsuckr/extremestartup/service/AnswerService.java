package cz.radimsuckr.extremestartup.service;

import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import cz.radimsuckr.extremestartup.dao.AnswerDao;
import cz.radimsuckr.extremestartup.model.Answer;
import cz.radimsuckr.extremestartup.model.Participant;

/**
 * Simple service class for Answer entity.
 */
@Stateless
public class AnswerService {

    @EJB
    private AnswerDao answerDao;

    public Long countCorrectAnswersInRow(Participant participant) {
        var level = answerDao.findCurrentLevelByParticipant(participant);
        var latestWrongAnswer = answerDao.findLatestWrongAnswerByParticipant(participant, level);
        return answerDao.countCorrectAnswersUntilAnswerIdByParticipant(participant, level,
                latestWrongAnswer == null ? 0 : latestWrongAnswer.getId());
    }

    public List<Answer> findCorrectByParticipantAndLevel(Participant participant, Long level) {
        return answerDao.findCorrectByParticipantAndLevel(participant, level);
    }

    public Long findLastQuestionIdByParticipant(Participant participant) {
        return answerDao.findLastQuestionIdByParticipant(participant);
    }

    public void persist(Answer answer) {
        answerDao.persist(answer);
    }

    public Map<String, Long> sumPoints() {
        return answerDao.sumPoints();
    }

    public long sumPointsByParticipant(Participant participant) {
        return answerDao.sumPointsByParticipant(participant);
    }
}
