package cz.radimsuckr.extremestartup.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import cz.radimsuckr.extremestartup.dao.AnswerDao;
import cz.radimsuckr.extremestartup.dao.ParticipantDao;
import cz.radimsuckr.extremestartup.dto.QuestionApiParticipantDto;
import cz.radimsuckr.extremestartup.exception.ParticipantOnSameIpAddressAndPortExists;
import cz.radimsuckr.extremestartup.exception.ParticipantWithSameUsernameExistsException;
import cz.radimsuckr.extremestartup.jsf.IndexBacking;
import cz.radimsuckr.extremestartup.jsf.IndexBacking.ParticipantData;
import cz.radimsuckr.extremestartup.model.Participant;

/**
 * Simple service class for Participant entity.
 */
@Stateless
public class ParticipantService {

    @EJB
    private AnswerDao answerDao;

    @EJB
    private ParticipantDao participantDao;

    public List<Participant> findAll() {
        return participantDao.findAll();
    }

    public Participant findById(Long id) {
        return participantDao.findById(id);
    }

    public List<Participant> findUnfinished() {
        return participantDao.findUnfinished();
    }

    public QuestionApiParticipantDto getQuestionServerMetadata(Participant participant) {
        var level = answerDao.findCurrentLevelByParticipant(participant);
        var answersCount = answerDao.countByParticipantAndLevel(participant, level);
        var correctAnswersCount = answerDao.countCorrectByParticipantAndLevel(participant, level);
        var latestWrongAnswer = answerDao.findLatestWrongAnswerByParticipant(participant, level);
        var correctAnswersInRowCount = answerDao.countCorrectAnswersUntilAnswerIdByParticipant(participant, level,
                latestWrongAnswer == null ? -1 : latestWrongAnswer.getId());
        return new QuestionApiParticipantDto(participant.getUsername(), level, correctAnswersCount, answersCount,
                correctAnswersInRowCount);
    }

    public void markAsFinished(Participant participant) {
        participant.markAsFinished();
        participantDao.merge(participant);
    }

    public void persist(Participant participant)
            throws ParticipantWithSameUsernameExistsException, ParticipantOnSameIpAddressAndPortExists {
        if (participantDao.countByUsername(participant.getUsername()) > 0) {
            throw new ParticipantWithSameUsernameExistsException();
        }
        if (participantDao.countByIpAddressAndPort(participant.getIpAddress(), participant.getPort()) > 0) {
            throw new ParticipantOnSameIpAddressAndPortExists();
        }

        participantDao.persist(participant);
    }

    public List<IndexBacking.ParticipantData> getParticipantDataListForIndexBacking() {
        return findAll().stream()
                .map(p -> new IndexBacking.ParticipantData(
                        String.format("%s (id: %s): level %s (%s)", p.getUsername(), p.getId(),
                                answerDao.findCurrentLevelByParticipant(p), answerDao.sumPointsByParticipant(p)),
                        p.isFinished()))
                .collect(Collectors.toList());
    }
}
