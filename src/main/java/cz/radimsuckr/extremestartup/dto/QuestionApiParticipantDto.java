package cz.radimsuckr.extremestartup.dto;

/**
 * DTO representing metadata about participant in Question server API.
 */
public class QuestionApiParticipantDto {

    private String name;

    private long level;

    private long correctAnswersCount;

    private long answersCount;

    private long correctAnswersInRowCount;

    public QuestionApiParticipantDto(String name, long level, long correctAnswersCount, long answersCount,
            long correctAnswersInRowCount) {
        this.name = name;
        this.level = level;
        this.correctAnswersCount = correctAnswersCount;
        this.answersCount = answersCount;
        this.correctAnswersInRowCount = correctAnswersInRowCount;
    }

    public String getName() {
        return name;
    }

    public long getLevel() {
        return level;
    }

    public long getCorrectAnswersCount() {
        return correctAnswersCount;
    }

    public long getAnswersCount() {
        return answersCount;
    }

    public long getCorrectAnswersInRowCount() {
        return correctAnswersInRowCount;
    }
}
