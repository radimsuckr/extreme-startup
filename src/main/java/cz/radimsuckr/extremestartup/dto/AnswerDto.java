package cz.radimsuckr.extremestartup.dto;

/**
 * DTO used for serialization of answers from participants when forwarding them
 * to question server for validation.
 */
public class AnswerDto {

    private Long questionId;

    private String payload;

    public AnswerDto(Long questionId, String payload) {
        this.questionId = questionId;
        this.payload = payload;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }
}
