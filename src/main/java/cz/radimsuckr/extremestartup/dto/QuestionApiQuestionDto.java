package cz.radimsuckr.extremestartup.dto;

/**
 * DTO representing a question response from question server.
 */
public class QuestionApiQuestionDto {

    private Long id;

    private Object content;

    private String expectedAnswers;

    private Long level;

    private Integer points;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

    public String getExpectedAnswers() {
        return expectedAnswers;
    }

    public void setExpectedAnswers(String expectedAnswers) {
        this.expectedAnswers = expectedAnswers;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return "QuestionApiQuestionDto{" + "id=" + id + ", content='" + content + '\'' + ", expectedAnswers='"
                + expectedAnswers + '\'' + ", level=" + level + '}';
    }
}
