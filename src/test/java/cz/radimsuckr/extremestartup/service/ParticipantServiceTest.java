package cz.radimsuckr.extremestartup.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import cz.radimsuckr.extremestartup.dao.ParticipantDao;
import cz.radimsuckr.extremestartup.exception.ParticipantOnSameIpAddressAndPortExists;
import cz.radimsuckr.extremestartup.exception.ParticipantWithSameUsernameExistsException;
import cz.radimsuckr.extremestartup.model.Participant;

@ExtendWith(MockitoExtension.class)
class ParticipantServiceTest {

    @Mock
    private ParticipantDao participantDao;

    @InjectMocks
    private ParticipantService participantService;

    @Test
    public void testPersistWithNewValidParticipantShouldNotThrowAnyException()
            throws ParticipantWithSameUsernameExistsException, ParticipantOnSameIpAddressAndPortExists {
        var participant = mock(Participant.class);
        when(participant.getUsername()).thenReturn("Pepek");
        when(participant.getIpAddress()).thenReturn("1.2.3.4");
        when(participant.getPort()).thenReturn(1234);

        when(participantDao.countByUsername(participant.getUsername())).thenReturn(0L);
        when(participantDao.countByIpAddressAndPort(participant.getIpAddress(), participant.getPort())).thenReturn(0L);

        assertDoesNotThrow(() -> participantService.persist(participant));
        verify(participantDao, times(1)).persist(participant);
    }

    @Test
    public void testPersistWithNewValidParticipantWithDuplicateUsernameShouldThrowParticipantWithSameUsernameExistsException()
            throws ParticipantWithSameUsernameExistsException, ParticipantOnSameIpAddressAndPortExists {
        var participant = mock(Participant.class);
        when(participant.getUsername()).thenReturn("Pepek");

        when(participantDao.countByUsername(participant.getUsername())).thenReturn(1L);

        assertThrows(ParticipantWithSameUsernameExistsException.class, () -> participantService.persist(participant));
        verify(participantDao, times(0)).persist(participant);
    }

    @Test
    public void testPersistWithNewValidParticipantWithDuplicateUsernameShouldThrowParticipantOnSameIpAddressAndPortExists()
            throws ParticipantWithSameUsernameExistsException, ParticipantOnSameIpAddressAndPortExists {
        var participant = mock(Participant.class);
        when(participant.getUsername()).thenReturn("Pepek");
        when(participant.getIpAddress()).thenReturn("1.2.3.4");
        when(participant.getPort()).thenReturn(1234);

        when(participantDao.countByUsername(participant.getUsername())).thenReturn(0L);
        when(participantDao.countByIpAddressAndPort(participant.getIpAddress(), participant.getPort())).thenReturn(1L);

        assertThrows(ParticipantOnSameIpAddressAndPortExists.class, () -> participantService.persist(participant));
        verify(participantDao, times(0)).persist(participant);
    }
}
