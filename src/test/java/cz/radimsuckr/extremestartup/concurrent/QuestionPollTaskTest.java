package cz.radimsuckr.extremestartup.concurrent;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.enterprise.event.Event;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import cz.radimsuckr.extremestartup.MemoryAppender;
import cz.radimsuckr.extremestartup.concurrent.event.HttpQuestionEvent;
import cz.radimsuckr.extremestartup.concurrent.event.SseQuestionEvent;
import cz.radimsuckr.extremestartup.dto.QuestionApiParticipantDto;
import cz.radimsuckr.extremestartup.dto.QuestionApiQuestionDto;
import cz.radimsuckr.extremestartup.exception.InvalidResponseFromQuestionServerException;
import cz.radimsuckr.extremestartup.exception.NoMoreQuestionsForParticipantException;
import cz.radimsuckr.extremestartup.model.CommunicationType;
import cz.radimsuckr.extremestartup.model.Participant;
import cz.radimsuckr.extremestartup.rest.QuestionApiClient;
import cz.radimsuckr.extremestartup.service.ParticipantService;

@ExtendWith(MockitoExtension.class)
class QuestionPollTaskTest {

    @Mock
    private ParticipantService participantService;

    @Mock
    private QuestionApiClient questionApiClient;

    @Mock
    private Event<SseQuestionEvent> sseQuestionEvent;

    @Mock
    private Event<HttpQuestionEvent> httpQuestionEvent;

    private MemoryAppender memoryAppender;

    private final static String LOGGER_NAME = QuestionPollTask.class.getName();

    @BeforeEach
    public void setupMemoryAppender() {
        var logger = (Logger) LoggerFactory.getLogger(LOGGER_NAME);
        memoryAppender = new MemoryAppender();
        memoryAppender.setContext((LoggerContext) LoggerFactory.getILoggerFactory());
        logger.setLevel(Level.DEBUG);
        logger.addAppender(memoryAppender);
        memoryAppender.start();
    }

    @Test
    public void testPollWithNullParticipantShouldThrowRuntimeException() {
        var participantId = 42L;

        var task = new QuestionPollTask(participantService, participantId, questionApiClient, sseQuestionEvent,
                httpQuestionEvent);

        var thrown = assertThrows(RuntimeException.class, () -> task.poll());
        assertEquals("Participant " + participantId + " does not exist", thrown.getMessage());
        assertEquals(1, memoryAppender.countEventsForLogger(LOGGER_NAME));
        assertTrue(memoryAppender.contains("Participant " + participantId + " does not exist", Level.ERROR));
    }

    @Test
    public void testPollWithUnfinishedParticipantShouldGetNextQuestionAndSendItToParticipant()
            throws NoMoreQuestionsForParticipantException, InvalidResponseFromQuestionServerException {
        var participantId = 42L;
        var participant = mock(Participant.class);
        when(participant.getId()).thenReturn(participantId);
        when(participant.getCommunicationType()).thenReturn(CommunicationType.SSE);
        when(participantService.findById(participantId)).thenReturn(participant);

        var questionId = 1337L;
        var question = mock(QuestionApiQuestionDto.class);
        when(question.getId()).thenReturn(questionId);

        var participantMetadata = mock(QuestionApiParticipantDto.class);
        when(participantService.getQuestionServerMetadata(participant)).thenReturn(participantMetadata);

        when(questionApiClient.getQuestion(participantMetadata)).thenReturn(question);

        var task = new QuestionPollTask(participantService, participantId, questionApiClient, sseQuestionEvent,
                httpQuestionEvent);

        assertDoesNotThrow(() -> task.poll());
        assertEquals(2, memoryAppender.countEventsForLogger(LOGGER_NAME));
        assertTrue(memoryAppender.contains("Participant " + participantId + " found", Level.INFO));
        assertTrue(memoryAppender.contains("Sending new SSE question " + questionId + " to " + participantId,
                Level.INFO));
    }

    @Test
    public void testPollWithFinishingParticipantShouldMarkThemAsFinishedAndLogAMessage()
            throws NoMoreQuestionsForParticipantException, InvalidResponseFromQuestionServerException {
        var participantId = 42L;
        var participant = mock(Participant.class);
        when(participant.getId()).thenReturn(participantId);
        when(participantService.findById(participantId)).thenReturn(participant);

        var participantMetadata = mock(QuestionApiParticipantDto.class);
        when(participantService.getQuestionServerMetadata(participant)).thenReturn(participantMetadata);

        when(questionApiClient.getQuestion(participantMetadata))
                .thenThrow(NoMoreQuestionsForParticipantException.class);

        var task = new QuestionPollTask(participantService, participantId, questionApiClient, sseQuestionEvent,
                httpQuestionEvent);

        assertDoesNotThrow(() -> task.poll());
        verify(participantService, times(1)).markAsFinished(participant);
        assertEquals(2, memoryAppender.countEventsForLogger(LOGGER_NAME));
        assertTrue(memoryAppender.contains("Participant " + participantId + " found", Level.INFO));
        assertTrue(memoryAppender.contains("No more questions for participant " + participantId, Level.INFO));
    }

    @Test
    public void testPollShouldLogErrorWhenQuestionServerReturnsInvalidResponse()
            throws NoMoreQuestionsForParticipantException, InvalidResponseFromQuestionServerException {
        var participantId = 42L;
        var participant = mock(Participant.class);
        when(participant.getId()).thenReturn(participantId);
        when(participantService.findById(participantId)).thenReturn(participant);

        var participantMetadata = mock(QuestionApiParticipantDto.class);
        when(participantService.getQuestionServerMetadata(participant)).thenReturn(participantMetadata);

        when(questionApiClient.getQuestion(participantMetadata))
                .thenThrow(InvalidResponseFromQuestionServerException.class);

        var task = new QuestionPollTask(participantService, participantId, questionApiClient, sseQuestionEvent,
                httpQuestionEvent);

        assertDoesNotThrow(() -> task.poll());
        assertEquals(2, memoryAppender.countEventsForLogger(LOGGER_NAME));
        assertTrue(memoryAppender.contains("Participant " + participantId + " found", Level.INFO));
        assertTrue(memoryAppender.contains("Question server returned invalid response", Level.ERROR));
    }

    @ParameterizedTest
    @EnumSource(CommunicationType.class)
    public void testSendNewQuestionWithAnyCommunicationTypeShouldReturnTrue(CommunicationType communicationType) {
        var participantId = 42L;
        var participant = mock(Participant.class);
        when(participant.getId()).thenReturn(participantId);
        when(participant.getCommunicationType()).thenReturn(communicationType);

        var questionId = 1337L;
        var question = mock(QuestionApiQuestionDto.class);
        when(question.getId()).thenReturn(questionId);

        var task = new QuestionPollTask(participantService, participantId, questionApiClient, sseQuestionEvent,
                httpQuestionEvent);

        task.sendNewQuestionEvent(participant, question);

        assertEquals(1, memoryAppender.countEventsForLogger(LOGGER_NAME));
    }
}
