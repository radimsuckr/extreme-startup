package cz.radimsuckr.extremestartup;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class UtilsTest {

    @ParameterizedTest
    @MethodSource("testCalculatePointsSource")
    public void testCalculatePoints(int points, boolean isCorrect, int result) {
        assertEquals(result, Utils.calculatePoints(points, isCorrect));
    }

    private static Stream<Arguments> testCalculatePointsSource() {
        return Stream.of(
                Arguments.of(20, true, 20),
                Arguments.of(17, false, -8),
                Arguments.of(20, false, -10),
                Arguments.of(Integer.MAX_VALUE, true, Integer.MAX_VALUE),
                Arguments.of(Integer.MAX_VALUE, false, -Integer.MAX_VALUE / 2));
    }
}
