# Extreme Startup

## Installation

The default address for question server is `http://localhost:5000`. However you can change this by creating a `microprofile-config.properties` file in `src/main/resources/META-INF` directory and setting the value of `questionServerUrl` to whatever you want.

```
questionServerUrl=http://mysecretdomain.local:1337/questionServer/
```

### Docker

Requirements for Docker:
 - Docker
 - Docker Compose

Steps to successfully start the application:
 1. `docker-compose up`
 2. Visit [http://localhost:8080/index.html](http://localhost:8080/index.html)

You will need to rebuild the Docker image for consecutive runs after changing application configs or source code. This can be done by running `docker-compose build` or `docker-compose up --build`. For a more seamless experience you can also mount a Docker volume, but this is beyond the scope of this readme file.

### Java

Requirements for Java:
 - Java 11

Steps to successfully start the application:
 1. `docker-compose up -d postgres`
 2. Set environment variables `POSTGRES_DB=app`, `POSTGRES_PASSWORD=app`, `POSTGRES_USER=app`
 3. Bundle Payara Micro (once) `mvn payara-micro:bundle`
 4. Start the Payara Micro server either via an IDE our through Maven (`./run.sh`)
 5. Visit [http://localhost:8080/index.html](http://localhost:8080/index.html)

## Demo implementation of question server and clients

For installation instructions of demo implementations please visit [example/python/readme.md](example/python/readme.md).
